DOCKER_COMPOSE  = docker-compose

EXEC_PHP        = $(DOCKER_COMPOSE) run --rm php
EXEC_NODE       = $(DOCKER_COMPOSE) run --rm node

SYMFONY         = $(EXEC_PHP) bin/console
COMPOSER        = $(EXEC_PHP) composer
NPM	            = $(EXEC_NODE) npm

## Application

build:
	touch docker/config/history
	$(DOCKER_COMPOSE) build

pull:
	touch docker/config/history
	$(DOCKER_COMPOSE) pull

start:
	$(DOCKER_COMPOSE) up -d --remove-orphans --force-recreate

start-debug:
	XDEBUG_MODE=debug $(DOCKER_COMPOSE) up -d --remove-orphans --force-recreate

stop:
	$(DOCKER_COMPOSE) down

vendor:
	$(COMPOSER) install

restart: stop start

kill:
	$(DOCKER_COMPOSE) kill
	$(DOCKER_COMPOSE) down --volumes --remove-orphans

php: ## Enter shell in php container
	$(DOCKER_COMPOSE) run --rm php bash

php-debug: ## Enter shell in php container
	XDEBUG_MODE=debug $(DOCKER_COMPOSE) run --rm php bash

cc: ## Clear the cache
	$(SYMFONY) cache:clear

db: vendor start                                            ## Reset the database
	@$(EXEC_PHP) php docker/php/wait-database.php
	$(SYMFONY) doctrine:database:drop --if-exists --force
	$(SYMFONY) doctrine:database:create --if-not-exists
	$(SYMFONY) doctrine:migrations:migrate --no-interaction --allow-no-migration
	$(SYMFONY) doctrine:fixtures:load --no-interaction --append
	$(SYMFONY) doctrine:schema:validate

install: pull start vendor db node_modules assets

update:                                                     ## Update composer dependencies
	$(COMPOSER) update
	git checkout -b dependencies/$(shell date +"%F-%H-%M")
	git add composer.lock
	git commit -m "Update dependencies - $(shell date +"%F-%H-%M")"
	git push origin dependencies/$(shell date +"%F-%H-%M")

.PHONY: build kill install start stop db migrate migration vendor restart php pull cs assets update

## Testing

db-test:                                                    ## Init the test database and load fixtures
	$(SYMFONY) doctrine:database:drop --if-exists --force --env=test
	$(SYMFONY) doctrine:database:create --if-not-exists --env=test
	$(SYMFONY) doctrine:migrations:migrate --no-interaction --allow-no-migration --env=test
	$(SYMFONY) doctrine:fixtures:load --no-interaction --append --env=test

test: db-test                                                ## Run all the test suite
	$(EXEC_PHP) bin/phpunit

.PHONY: db-test test

## Assets

node_modules: package-lock.json
	$(NPM) ci
	@touch -c node_modules

package-lock.json: package.json
	$(NPM) install

node:                                                        ## Enter shell in node container
	$(DOCKER_COMPOSE) run --rm node bash

assets:                                                      ## Compile assets
	$(NPM) run dev

watch:                                                       ## Watch assets
	$(NPM) run watch

.PHONY: assets watch

## QA

cs:                                                          ## Run php-cs-fixer
	$(EXEC_PHP) ./vendor/bin/php-cs-fixer fix --verbose --diff --show-progress=dots

psalm:                                                       ## Run Psalm
	$(EXEC_PHP) ./vendor/bin/psalm --show-info=true

dust-psalm:                                                  ## Update Psalm baseline
	$(EXEC_PHP) ./vendor/bin/psalm --update-baseline

baseline-psalm:                                              ## Generate Psalm baseline
	$(EXEC_PHP) ./vendor/bin/psalm --set-baseline=docker/php/psalm-baseline.xml

rector:                                                      ## Run Rector
	$(EXEC_PHP) ./vendor/bin/rector

security:
	$(EXEC_PHP) ./bin/local-php-security-checker

.PHONY: cs psalm dust-psalm baseline-psalm rector security

## HELP
help:                                                        ## show the help
	@grep -E '(^[0-9a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.DEFAULT_GOAL := help