<?php

namespace App\Constraints;

use PHPUnit\Framework\Constraint\Constraint;
use Symfony\Component\HttpFoundation\Request;

class CrawlerTaskExists extends Constraint
{
    private $filter;
    private $nodeValue;

    public function __construct(string $filter, string $nodeValue)
    {
        $this->filter = $filter;
        $this->nodeValue = $nodeValue;
    }

    /**
     * {@inheritdoc}
     */
    public function toString(): string
    {
        return sprintf('filtered by "%s" contains the task "%s"', $this->filter, $this->nodeValue);
    }

    /**
     * @param Request $request
     *
     * {@inheritdoc}
     */
    protected function matches($crawler): bool
    {
        $elems = $crawler->filter($this->filter);
        foreach($elems as $elem) {
            /* @var \DOMElement $elem */
            if ($elem->nodeValue === $this->nodeValue) {
                return true;
            }
        }
        return false;

    }

    /**
     * @param Request $request
     *
     * {@inheritdoc}
     */
    protected function failureDescription($request): string
    {
        return 'the Crawler '.$this->toString();
    }
}