<?php

namespace App\EventListener;

use App\Entity\Tag;
use App\Entity\Task;
use App\Repository\TagRepository;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AddTagger
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function prePersist(Task $task, LifecycleEventArgs $event)
    {
        $user = $this->tokenStorage->getToken()?->getUser();
        if ($user) {
            $task->setUser($user);
        }
        $entityManager = $event->getObjectManager();
        $tagRepository = $entityManager->getRepository(Tag::class);

        foreach ($task->getTags() as $tag) {
            $existingTag = $tagRepository->findOneBy(['name' => $tag->getName()]);
            if ($existingTag !== null) {
                $task->removeTag($tag);
                $task->addTag($existingTag);
            } else {
                $entityManager->persist($tag);
            }
        }
    }
}