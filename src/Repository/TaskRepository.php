<?php

namespace App\Repository;

use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Task>
 *
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }

    public function add(Task $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Task $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
    public function getTasksFilteredByTag(int $userId, ?array $filterTags)
    {
        $qb = $this->createQueryBuilder('task')
            ->join('task.user','user')
            ->where('user.id = :uid')
            ->setParameter('uid',$userId);
            if(null !== $filterTags){
                $qb
                    ->join('task.tags','tags')
                    ->andWhere('tags.name in (:filterTags)')
                    ->setParameter('filterTags',$filterTags);
            }





      //Tentative de réparation du filtre, il faut ajouter un userRepo dans la signature
            /*      $arrayResult = $qb->distinct(true)->getQuery()->getArrayResult();
        foreach($arrayResult as $ftask) {
                    $task = new Task();
                    $task->setId($ftask['id']);
                    $task->setName($ftask['name']);
                    $task->setStatus($ftask['status']);
                    $task->setUser($userRepository->findOneBy(['id' => $userId],));
                    $taskArray[] = $task;
                }

            foreach($taskArray as $task){

                dd($task);
                if($task->getTags()->getValues() == $filterTags){

                }
            }*/
        return $qb->distinct(true)->getQuery()->getResult();
    }

    public function getTasksFilteredByName(int $userId, ?string $filterTask)
    {
        $qb = $this->createQueryBuilder('task')
            ->join('task.user','user')
            ->where('user.id = :uid')
            ->setParameter('uid',$userId);
        if(null !== $filterTask){
            $qb
                ->andWhere('task.name like :filterTask')
                ->setParameter('filterTask', '%'.$filterTask.'%');
        }
        return $qb->distinct(true)->getQuery()->getResult();
    }

    public function getTasksFromCurrentUser(int $uid)
    {
        $qb = $this->createQueryBuilder('task')
            ->join('task.user','user')
            ->where('user.id = :uid')
            ->setParameter('uid',$uid);
        return $qb->getQuery()->getResult();
    }
//    /**
//     * @return Task[] Returns an array of Task objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Task
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
