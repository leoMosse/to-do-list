<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Factory\UserFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        UserFactory::createMany(3);
        // Fix user
        UserFactory::createOne([
            'email' => 'admin@widop.com',
            'roles' => ['ROLE_USER','ROLE_ADMIN']
        ]);
    }
}
