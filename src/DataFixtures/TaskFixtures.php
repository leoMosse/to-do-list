<?php
namespace App\DataFixtures;


use App\Entity\Tag;
use App\Factory\TaskFactory;
use App\Factory\UserFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker;

class TaskFixtures extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create();
        $tags = [];
        for($i=0; $i<10 ; $i++){
            $tag = new Tag();
            $tag->setName($faker->text(10));
            $tags[] = $tag;
        }
        foreach (UserFactory::all() as $user) {
            for ($i = 0 ; $i<20 ; $i++){
                $randomValue = random_int(0,3);

                TaskFactory::createOne([
                    'tags' => $randomValue ? array_intersect_key($tags, $randomValue === 1 ? [ array_rand($tags, $randomValue) => 0 ] : array_flip(array_rand($tags, $randomValue))) : [],                    'user' => $user,
                ]);
            }
        }

    }
    public function getDependencies()
    {
        return [
            UserFixtures::class,
            TagFixtures::class,
        ];
    }
}
