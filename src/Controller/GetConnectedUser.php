<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

#[AsController]
class GetConnectedUser extends AbstractController
{
    public function __construct(private UserRepository $userRepository, private TokenStorageInterface $tokenStorage){}
    public function __invoke(): array
    {
        $data = $this->userRepository->getConnectedUser($this->tokenStorage);
        return $data;
    }
}