<?php

namespace App\Controller;

use App\Entity\Task;
use App\Form\SearchTaskType;
use App\Form\TaskFormType;
use App\Repository\TagRepository;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Workflow\WorkflowInterface;


class TaskController extends AbstractController
{

    private WorkflowInterface $taskStateMachine;

    public function __construct(WorkflowInterface $taskStateMachine)
    {
        $this->taskStateMachine = $taskStateMachine;
    }

    #[Route('/dashboard', name: 'app_task')]
    public function index(Request $request, RequestStack $session, EntityManagerInterface $entityManager, TagRepository $tagRepository, UserRepository $userRepository, TaskRepository $taskRepository): Response
    {
        $task = new Task();
        $form = $this->createForm(TaskFormType::class, $task);
        $form->handleRequest($request);


        $userId = $userRepository->findOneBy(['email' => $this->getUser()->getUserIdentifier()])->getId();
        $tags = $tagRepository->getEachDistinctTagFromUser($userId);
        $filterTags = $request->get('filter-tags');

        $searchBarForm = $this->createForm(SearchTaskType::class,);
        $searchBarForm->handleRequest($request);

        $session = $session->getSession(); //La session
        $search = $session->get('search',null); //La dernière recherche effectuée durant la session, null par défaut


        if ($searchBarForm->isSubmitted() && $searchBarForm->isValid()) {
            $session->set('search',$searchBarForm->getData()->getName());
        }else{
            $searchBarForm->get('name')->setData($search);
        }


        $tasks = array_intersect($taskRepository->getTasksFilteredByName($userId,$search) , $taskRepository->getTasksFilteredByTag($userId,$filterTags));

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($task);
            $entityManager->flush();
            return $this->redirectToRoute('app_task');
        }
        return $this->renderForm('task/tasks.html.twig', ['form' => $form,'searchBarForm' => $searchBarForm, 'allTasks' => $tasks ,'individualTags' => $tags]);
    }

    #[Route('/moveTask/{id}/{transition}', name: 'app_move_task')]
    #[ParamConverter('task', class: Task::class)]
    public function move(Task $task, string $transition, EntityManagerInterface $entityManager): Response
    {
        if ($this->taskStateMachine->can($task, $transition)) {
            $this->taskStateMachine->apply($task, $transition);
            $entityManager->persist($task);
            $entityManager->flush();
            return $this->redirectToRoute("app_task");
        }
    }

    #[Route('/deleteTask/{id}', name: 'app_delete_task')]
    public function delete(string $id, EntityManagerInterface $entityManager, TaskRepository $taskRepo): Response
    {
        $task = $taskRepo->findOneBy(['id' => $id]);
        if($task !== null) {
            foreach ($task->getTags() as $tag) {
                $tag->removeTaggedTask($task);
                if ($tag->getTaggedTasks()->isEmpty()) {
                    $entityManager->remove($tag);
                }
            }
        }
        $entityManager->remove($task);
        $entityManager->flush();
        return $this->redirectToRoute("app_task");
    }

    #[Route('/editTask/{id}', name: 'app_edit_task')]
    public function edit(string $id, EntityManagerInterface $entityManager, TaskRepository $taskRepo, Request $request, UserRepository $userRepository, TagRepository $tagRepository): Response
    {
        $task = $taskRepo->findOneBy(['id' => $id]);
        $form = $this->createForm(TaskFormType::class, $task);
        $userId = $userRepository->findOneBy(['email' => $this->getUser()->getUserIdentifier()])->getId();
        $tags = $tagRepository->getEachDistinctTagFromUser($userId);
        $form->handleRequest($request);

        $searchBarForm = $this->createForm(SearchTaskType::class,);
        $searchBarForm->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($task);
            $entityManager->flush();
            return $this->redirectToRoute("app_task");
        }
        return $this->renderForm('task/tasks.html.twig', ['form' => $form, 'searchBarForm' => $searchBarForm, 'allTasks' => $this->getUser()->getTasks(), 'individualTags' => $tags]);
    }

}
