<?php

namespace App\Controller;

use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class GetTasksFromUser extends AbstractController
{
    public function __construct(private TaskRepository $taskRepository, private UserRepository $userRepository){}
    public function __invoke(): array
    {
        $uid = $this->userRepository->findOneBy(['email' => $this->getUser()->getUserIdentifier()])->getId();
        $data = $this->taskRepository->getTasksFromCurrentUser($uid);
        return $data;
    }
}