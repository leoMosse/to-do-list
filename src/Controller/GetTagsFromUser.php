<?php
namespace App\Controller;

    use App\Repository\TagRepository;
    use App\Repository\UserRepository;
    use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
    use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class GetTagsFromUser extends AbstractController
{
    public function __construct(private TagRepository $tagRepository, private UserRepository $userRepository)
    {
    }

    public function __invoke(): array
    {
        $uid = $this->userRepository->findOneBy(['email' => $this->getUser()->getUserIdentifier()])->getId();
        $data = $this->tagRepository->getEachDistinctTagFromUser($uid);
        return $data;
    }
}
