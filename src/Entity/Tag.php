<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\GetTagsFromUser;
use App\Repository\TagRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

//Récupérer uniquement les tags liés aux tâches du user actuel
#[ORM\Entity(repositoryClass: TagRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get_custom" => [
            "security" => "is_granted('ROLE_USER')",
            'method' => 'GET',
            'path' => '/tags',
            'controller' => GetTagsFromUser::class,
        ],
        "post" => [
            "security" => "is_granted('ROLE_USER')",
        ],
    ],
    itemOperations: [
        "get" => ["security" => "is_granted('ROLE_USER')" ],

    ],
    attributes: ["security" => "is_granted('ROLE_USER')"],
)]
#[ORM\UniqueConstraint(name: 'name', columns: ['name'])]
class Tag
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 40), ]
    private $name;

    #[ORM\ManyToMany(targetEntity: Task::class, inversedBy: 'tags')]
    private $taggedTasks;

    public function __construct()
    {
        $this->taggedTasks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Task>
     */
    public function getTaggedTasks(): Collection
    {
        return $this->taggedTasks;
    }

    public function addTaggedTask(Task $taggedTask): self
    {
        if (!$this->taggedTasks->contains($taggedTask)) {
            $this->taggedTasks[] = $taggedTask;
        }
        return $this;
    }

    public function removeTaggedTask(Task $taggedTask): self
    {
        $this->taggedTasks->removeElement($taggedTask);

        return $this;
    }
    public function __toString(): string
    {
        return $this->getName();
    }
}
