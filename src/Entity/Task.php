<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\EventListener\AddTagger;
use App\Repository\TaskRepository;
use App\Controller\GetTasksFromUser;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\EntityListeners;

#[EntityListeners([AddTagger::class])]
#[ORM\Entity(repositoryClass: TaskRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get_custom" => [
            "security" => "is_granted('ROLE_USER')",
            'method' => 'GET',
            'path' => '/tasks',
            'controller' => GetTasksFromUser::class,
            ],
        "post" => [
            "security" => "is_granted('ROLE_USER')",
            'path' => '/tasks',
            'openapi_context' => [
                'summary' => 'Creates a task',
                'requestBody' => [
                    'content' => [
                        'application/json' => [
                            'schema'  => ["name"=>"string","tags"=>["string"]],
                            'example' => ["name"=>"string","tags"=>["string"]],
                        ]
                    ]
                ]
            ]
        ],
    ],
    itemOperations: [
        "get" => ["security" => "object.user == user" ],
        "delete" => ["security" => "object.user == user" ],
        "patch" => ["security" => "object.user == user" ],
    ],
    attributes: ["security" => "is_granted('ROLE_USER')"],
)]
class Task
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'string', length: 255)]
    private $status = 'Todo';

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'tasks')]
    #[ORM\JoinColumn(nullable: false)]
    private $user;

    #[ORM\ManyToMany(targetEntity: Tag::class, mappedBy: 'taggedTasks', cascade: ['persist'])]
    private $tags;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }
    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;
        $user->addTask($this);

        return $this;
    }

    /**
     * @return Collection<int, Tag>
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $tagsWithCurrentName = $this->getTags()->filter(function (Tag $tagItem) use ($tag) {
                return $tagItem->getName() === $tag->getName();
            });
            if($tagsWithCurrentName->count() === 0) {
                $this->tags[] = $tag;
                $tag->addTaggedTask($this);
            }
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->removeElement($tag)) {
            $tag->removeTaggedTask($this);
            $this->tags->removeElement($tag);

        }

        return $this;
    }
    public function __toString(): string
    {
        return $this->getName();
    }
}
