<?php

namespace App\Tests;

use App\Constraints\CrawlerTaskExists;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\BrowserKit\AbstractBrowser;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Panther\Client;
use Symfony\Component\Panther\PantherTestCase;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use function func_num_args;

class TaskTest extends WebTestCase
{
    private $client = null;

    public function setUp(): void
    {
    }

    public function testAddTaskWithNoTag(): void
    {
        $this->client = static::createClient();
        /** @var User $user */
        $userRepository = static::getContainer()->get(UserRepository::class);
        $user = $userRepository->findAll()[0];
        $this->client->loginUser($user);
        $this->client->request('GET', '/dashboard');
        //$client->followRedirect();
        //sleep(2);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1','To-do list');
        $this->client->submitForm('task_form_submit',['task_form[name]' => 'TESTESTESTESTESTESTESTEST']);
        //sleep(2);
        $crawler = $this->client->request('GET', '/dashboard');
        //$this->client->waitForVisibility('div.row-cols-3 > div:first-child > div strong');

        $this->assertTaskCreated($crawler,'div.row-cols-3 > div:first-child > div strong','TESTESTESTESTESTESTESTEST');;
        dump($crawler->filter('script'));
        /* foreach($elems as $elem) {
            //@var \DOMElement $elem
            if ($elem->nodeValue === 'TESTESTESTESTESTESTESTEST') {
                dump('trouvé');
                dump($elem);
            } else {
                dump('not found');
            }
        }*/
    }

    public function assertTaskCreated(Crawler $crawler, string $filter, string $nodeValue)
    {
        self::assertThat($crawler, new CrawlerTaskExists($filter, $nodeValue));
    }
}
